import { initializeApp } from "https://www.gstatic.com/firebasejs/10.4.0/firebase-app.js";
import { getFirestore, collection, getDocs, addDoc, doc, deleteDoc, updateDoc, getDoc, setDoc } from "https://www.gstatic.com/firebasejs/10.4.0/firebase-firestore.js";
import { getAuth } from "https://www.gstatic.com/firebasejs/10.4.0/firebase-auth.js"; // Importa getAuth desde firebase-auth.js

const firebaseConfig = {
  apiKey: "AIzaSyCzCN_69meoMTP9mAL4yDJcXFVEA3Pq0dA",
  authDomain: "clientes-26237.firebaseapp.com",
  databaseURL: "https://clientes-26237-default-rtdb.firebaseio.com",
  projectId: "clientes-26237",
  storageBucket: "clientes-26237.appspot.com",
  messagingSenderId: "320420249976",
  appId: "1:320420249976:web:d2962562d9377241abc88d"
};

const app = initializeApp(firebaseConfig);
// Inicializar Firebase Firestore
const db = getFirestore(app);
const auth = getAuth(app);

export const saveForm = (dni, name, surname, date, address, phone, email, obraSocial, description) => {
  return addDoc(collection(db, 'clientes'), { dni, name, surname, date, address, phone, email, obraSocial, description }
  )
}

export const consultaForm = (clienteId, fechaConsulta, altura, peso, imc, detalles) => {
  return addDoc(collection(db, 'clientes', clienteId, 'consultas'), { fechaConsulta, altura, peso, imc, detalles }
  )
}

export const getForm = async () => {
  const querySnapshot = await getDocs(collection(db, 'clientes'));
  return querySnapshot;
};
export const getConsulta = async (clienteId) => {
  const querySnapshot = await getDocs(collection(db, 'clientes', clienteId, 'consultas'));
  return querySnapshot;
};


export const deleteCliente = async (clienteId) => {
  try {
    const clienteRef = doc(db, "clientes", clienteId);
    await deleteDoc(clienteRef);
    console.log("Cliente eliminado correctamente");
  } catch (error) {
    console.error("Error al eliminar el cliente:", error);
  }
};

export const deleteConsulta = async (clienteId, consultasId) => {
  try {
    const clienteRef = doc(db, "clientes", clienteId, 'consultas', consultasId);
    await deleteDoc(clienteRef);
    console.log("Consulta eliminado correctamente");
  } catch (error) {
    console.error("Error al eliminar la consulta:", error);
  }
};


export const updateCliente = async (clienteId, newData) => {
  const clienteRef = doc(db, "clientes", clienteId);

  try {
    await updateDoc(clienteRef, newData);
    console.log("Cliente actualizado con éxito");
  } catch (error) {
    console.error("Error al actualizar el cliente:", error);
  }
};

export const getCliente = async (clienteId) => {
  const clienteRef = doc(db, "clientes", clienteId);
  const clienteSnapshot = await getDoc(clienteRef);

  if (clienteSnapshot.exists()) {
    return clienteSnapshot.data();
  } else {
    console.error("Cliente no encontrado");
    return null;
  }
};

export const getHistorial = async (clienteId, consultasId) => {
  if (clienteId && consultasId) { // Comprueba que ambos valores no sean undefined
    const consultaRef = doc(db, 'clientes', clienteId, 'consultas', consultasId);
    const consultaSnapshot = await getDoc(consultaRef);
    console.log(consultasId)
    if (consultaSnapshot.exists()) {
      return consultaSnapshot.data();
    } else {
      console.error("Consulta no encontrada");
      return null; // Retorna null para indicar que la consulta no se encontró
    }
  } else {
    console.error("Valores de clienteId o consultasId indefinidos");
    return null; // Retorna null en caso de valores indefinidos
  }
};





export { app, db, doc, auth, setDoc }