import { getConsulta, consultaForm, getHistorial, deleteConsulta } from "../firebase.js";

// Obtiene y muestra el nombre y apellido
const urlParams = new URLSearchParams(window.location.search);
const apellidoCliente = urlParams.get("apellido");
const nombreCliente = urlParams.get("nombre");
const dniCliente = urlParams.get("dni");
const dateCliente = urlParams.get("date");
const osCliente = urlParams.get("obraSocial");


function calcularEdad(fechaNacimiento) {
  const fechaNac = new Date(fechaNacimiento);
  const fechaActual = new Date();

  let edad = fechaActual.getFullYear() - fechaNac.getFullYear();
  const mesActual = fechaActual.getMonth() + 1;
  const mesNacimiento = fechaNac.getMonth() + 1;

  if (mesActual < mesNacimiento || (mesActual === mesNacimiento && fechaActual.getDate() < fechaNac.getDate())) {
    edad--;
  }

  return edad;
}

// Obtener la fecha de nacimiento desde clientesData.date 
const fechaNacimientoCliente = dateCliente; // Reemplaza esto con la fecha de nacimiento real

// Calcular la edad
const edadCliente = calcularEdad(fechaNacimientoCliente);

// Encuentra el elemento donde deseas mostrar el nombre y establece su contenido
const customerElement = document.getElementById("customer");
customerElement.textContent = `${apellidoCliente} ${nombreCliente} - ${edadCliente} Años - DNI: ${dniCliente} - ${osCliente}`;

// Botón volver
const botonVolver = document.querySelector(".buttom-back");
botonVolver.addEventListener("click", function () {
  window.location.href = "../tableCustomers.html";
});

document.addEventListener("DOMContentLoaded", function () {
  const urlParams = new URLSearchParams(window.location.search);
  const form = document.getElementById("form-control");

  // Verifica si se encontró el formulario
  if (form) {
    const alturaInput = form.querySelector('input[name="altura"]');
    const pesoInput = form.querySelector('input[name="peso"]');
    const imcInput = document.getElementById("imcInput");

    // Agrega un evento 'input' a los campos de altura y peso
    alturaInput.addEventListener("input", calcularIMC);
    pesoInput.addEventListener("input", calcularIMC);

    function calcularIMC() {
      const altura = parseFloat(alturaInput.value);
      const peso = parseFloat(pesoInput.value);

      if (!isNaN(altura) && !isNaN(peso) && altura > 0) {
        const imc = peso / (altura * altura);
        imcInput.value = imc.toFixed(2);
      } else {
        imcInput.value = ""; // Borra el campo IMC si los valores no son válidos
      }
    }

    // Agrega el evento submit al formulario
    form.addEventListener("submit", async (e) => {
      e.preventDefault(); // Evitar el envío automático del formulario

      const fechaConsulta = form.querySelector('input[type="date"]').value;
      const altura = form.querySelector('input[name="altura"]').value;
      const peso = form.querySelector('input[name="peso"]').value;
      const imc = parseFloat(imcInput.value); // Obtén el valor del campo IMC
      const detalles = form.querySelector('textarea[name="detalles"]').value;

      const clienteId = urlParams.get("clienteId");

      if (clienteId) {
        // Utiliza la función consultaForm para agregar una consulta
        await consultaForm(clienteId, fechaConsulta, altura, peso, imc, detalles);

        form.reset();
        window.location.reload();
      } else {
        console.error("ID del cliente no encontrado");
      }
    });
  } else {
    console.error("Formulario no encontrado en el DOM");
  }
});


// Obtiene la URL actual
const url = new URL(window.location.href);

// Obtiene el valor del parámetro "clienteId" de la URL
const clienteId = url.searchParams.get("clienteId");

const clientesTable = document.getElementById("tableCliente");
//carga la tabla con datos
function updateTable(consultaDataList) {
  let html = "<thead><tr>";
  const columnNames = [
    "Fecha",
    "Altura",
    "Peso",
    "IMC",
    "Detalles",
    "Acciones"
  ];
  columnNames.forEach((columnName) => {
    html += `<th>${columnName}</th>`;
  });
  html += "</tr></thead><tbody>";
  consultaDataList.forEach((consultaData) => {
    // Formatear la fecha como día/mes/año
    const fechaConsulta = new Date(consultaData.fechaConsulta);
    const formattedFecha = fechaConsulta.toLocaleDateString('es-ES');
    // Truncar el contenido de la columna "Detalles" a una línea
    const detalles = consultaData.detalles.length > 20 ? consultaData.detalles.substring(0, 20) + '...' : consultaData.detalles;
    html += `        
            <tr>
                <td>${formattedFecha}</td>
                <td>${consultaData.altura} cm</td>
                <td>${consultaData.peso} kg</td>
                <td>${consultaData.imc}</td>
                <td>${detalles}</td>                
                <td>
                  <button type="button" class="btn btn-success button-view" data-id="${consultaData.id}" data-bs-toggle="modal" data-bs-target="#viewConsulta">
                    <i class="fas fa-sharp fa-solid fa-eye"></i>
                  </button>
                  <button type="button" class="btn btn-danger button-delete" data-id="${consultaData.id}">
                    <i class="fas fa-trash"></i>
                  </button>
                </td>
            </tr>
        `;
  });
  html += "</tbody>";
  clientesTable.innerHTML = html;

  const buttonView = clientesTable.querySelectorAll(".button-view");
  buttonView.forEach((btn) => {
    btn.addEventListener("click", async (e) => {
      const consultasId = e.currentTarget.getAttribute("data-id");
      const consultaData = await getHistorial(clienteId, consultasId);
      if (consultaData !== null) {
        // Mostrar los datos de la consulta en un modal
        showConsultaModal(consultaData);
      } else {
        console.log("Consulta no encontrada");
      }
    });
  });

  const buttonDelete = clientesTable.querySelectorAll(".button-delete");
  buttonDelete.forEach((btn) => {
    btn.addEventListener("click", async (e) => {
      const consultasId = e.currentTarget.getAttribute("data-id");
      await deleteConsulta(clienteId, consultasId);
      const updatedQuerySnapshot = await getConsulta(clienteId);
      showNotification("Consulta eliminada correctamente");
      const consultaDataList = updatedQuerySnapshot.docs.map((doc) => {
        const consultaData = doc.data();
        return { ...consultaData, id: doc.id };
      });

      consultaDataList.sort((a, b) => {
        const fechaA = new Date(a.fechaConsulta);
        const fechaB = new Date(b.fechaConsulta);
        return fechaB - fechaA;
      });
      updateTable(consultaDataList);
    });
  });
  // Función para mostrar la notificación
  function showNotification(message) {
    const notificationElement = document.getElementById("notification");
    notificationElement.textContent = message;

    // Agrega estilos de diseño o clases 
    notificationElement.style.backgroundColor = "#08C706"; // Fondo verde
    notificationElement.style.color = "white"; // Texto blanco
    notificationElement.style.fontSize = "30px";

    // Muestra la notificación por 3 segundos
    setTimeout(() => {
      notificationElement.textContent = "";
    }, 3000);
  }
  // Esta función muestra los datos de la consulta en un modal
  function showConsultaModal(consultaData) {
    const fechaConsultaElement = document.getElementById("fechaConsulta");
    const alturaElement = document.getElementById("altura");
    const pesoElement = document.getElementById("peso");
    const imcElement = document.getElementById("imc");
    const detallesElement = document.getElementById("detalles");

    fechaConsultaElement.textContent = consultaData.fechaConsulta;
    alturaElement.textContent = consultaData.altura;
    pesoElement.textContent = consultaData.peso;
    imcElement.textContent = consultaData.imc;
    detallesElement.textContent = consultaData.detalles;

    const consultaModal = document.getElementById("viewConsulta");
    consultaModal.classList.add("is-active");
  }
}

window.addEventListener("DOMContentLoaded", async (e) => {
  try {
    const querySnapshot = await getConsulta(clienteId); // Obtiene los datos de las consultas
    const consultaDataList = querySnapshot.docs.map((doc) => {
      const consultaData = doc.data();
      return { ...consultaData, id: doc.id };
    });
    // Ordenar los datos por fecha en orden descendente
    consultaDataList.sort((a, b) => {
      const fechaA = new Date(a.fechaConsulta);
      const fechaB = new Date(b.fechaConsulta);
      return fechaB - fechaA;
    });
    updateTable(consultaDataList); // Actualiza la tabla con los datos de las consultas ordenados
  } catch (error) {
    console.error("Error al obtener datos de las consultas:", error);
  }
});
